variable "name" {}
variable "rg_name" {}
variable "rg_vnet" {}
variable "size" {}
variable "snet_name" {}
variable "vnet_name" {}
variable "vnet_rg" {}
variable "admin" { sensitive = true }
variable "pwd" { sensitive = true }